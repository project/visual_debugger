/**
 * Delivers reusable code and resources across the application.
 */
Drupal.vdUtilities = {
  // Element IDs.
  ids: {
    idControllerSelectedElementInfo: "visual-debugger--controller--info",
    idControllerSelectedElementSuggestionsValue:
      "visual-debugger--controller--suggestions",
    idControllerSelectedElementFilePathValue:
      "visual-debugger--controller--file-path",
    idControllerSelectedElementCacheHit:
      "visual-debugger--controller--cache-hit",
    idControllerSelectedElementCacheMaxAge:
      "visual-debugger--controller--cache-max-age",
    idControllerSelectedElementCacheTags:
      "visual-debugger--controller--cache-tags",
    idControllerSelectedElementCacheContexts:
      "visual-debugger--controller--cache-contexts",
    idControllerSelectedElementCacheKeys:
      "visual-debugger--controller--cache-keys",
    idControllerSelectedElementPreBubblingCacheTags:
      "visual-debugger--controller--pre-bubbling-cache-tags",
    idControllerSelectedElementPreBubblingCacheContexts:
      "visual-debugger--controller--pre-bubbling-cache-contexts",
    idControllerSelectedElementPreBubblingCacheKeys:
      "visual-debugger--controller--pre-bubbling-cache-keys",
    idControllerSelectedElementPreBubblingCacheMaxAge:
      "visual-debugger--controller--pre-bubbling-cache-max-age",
    idControllerSelectedElementRenderingTime:
      "visual-debugger--controller--rendering-time"
  },

  // Class names.
  classNames: {
    classNameIconCheckboxChecked: "icon-checkbox-checked",
    classNameIconCheckboxUnchecked: "icon-checkbox-unchecked",
    classNameCheckboxToggleWrapper: "checkbox-toggle-wrapper",
    classNameCheckboxToggle: "checkbox-toggle",
    classNameIconWithinContent: "icon-within-content",
    classNameInputActivated: "item-activated",
    classNameInputDeactivated: "item-deactivated",
    classNameInputWrapperActivated: "wrapper-activated",
    classNameInputWrapperDeactivated: "wrapper-deactivated",
    classNameInputWrapperDisabled: "disabled",
    classNameSelectedElementInfoWrapper: "content-item__info-wrapper",
    classNameSelectedElementInfo: "content-item__info",
    classNameSelectedElementSuggestionsWrapper:
      "content-item__suggestions-wrapper",
    classNameSelectedElementSuggestions: "content-item__suggestions",
    classNameSelectedElementTemplateFilePathWrapper:
      "content-item__template-file-path-wrapper",
    classNameSelectedElementTemplateFilePath:
      "content-item__template-file-path",
    classNameSelectedElementTemplateFilePathLabel: "label",
    classNameObjectType: "object-type",
    classNameObjectTypeHover: "object-type--hover",
    classNameObjectTypeTyped: objectType => `object-type--${objectType}`
  },

  // layerAttributes.
  layerAttributes: {
    layerTargetIdAttributeName: "data-vd-target-id",
    listItemActivatedAttributeName: "data-vd-list-item-activated",
    instanceLayerActivatedAttributeName: "data-vd-instance-layer-activated",
    layerAttributeIsVisible: "data-vd-visible"
  },

  // Strings.
  strings: {
    stringThemeElementInfo: Drupal.t("Object Type"),
    stringThemeElementThemeSuggestions: Drupal.t("Theme Suggestions"),
    stringThemeElementFilePath: Drupal.t("Template File Path"),
    stringThemeElementFilePathInlineLabel: Drupal.t("File Path"),
    stringThemeElementCacheHit: Drupal.t("Cache Hit"),
    stringThemeElementCacheMaxAge: Drupal.t("Cache Max-Age"),
    stringThemeElementCacheTags: Drupal.t("Cache Tags"),
    stringThemeElementCacheContexts: Drupal.t("Cache Contexts"),
    stringThemeElementCacheKeys: Drupal.t("Cache Keys"),
    stringThemeElementPreBubblingCacheTags: Drupal.t("Pre-Bubbling Cache Tags"),
    stringThemeElementPreBubblingCacheContexts: Drupal.t(
      "Pre-Bubbling Cache Contexts"
    ),
    stringThemeElementPreBubblingCacheKeys: Drupal.t("Pre-Bubbling Cache Keys"),
    stringThemeElementPreBubblingCacheMaxAge: Drupal.t(
      "Pre-Bubbling Cache Max-Age"
    ),
    stringThemeElementRenderingTime: Drupal.t("Rendering Time")
  },

  /**
   * Gets all siblings of a given element.
   * @param {object} element
   *   The element for which to find siblings.
   * @return {array}
   *   An array of sibling elements.
   */
  getSiblings(element) {
    const parent = element.parentNode;
    const children = Array.from(parent.children);
    return children.filter(child => child !== element);
  },

  // Setter methods.
  generateUniqueIdentifier: () =>
    `element-${Math.random()
      .toString(36)
      .substring(7)}`,

  /**
   * Generates an on/off switch with a callback event on 'change'.
   * @param {string} label
   *   The label for the switch.
   * @param {boolean} activated
   *   Sets the default initial state.
   * @param {array} eventListeners
   *   An array of object pairs consisting on listeners and callbacks.
   * @param {object} wrapperAttributesList
   *   The attributes to be added to the wrapper div.
   * @param {array} wrapperClassList
   *   The classes to be added to the wrapper div.
   * @param {boolean} inputFirst
   *   If true, the input element will be placed before the label.
   * @param {string} iconOn
   *   The class name for the icon when activated.
   * @param {string} iconOff
   *   The class name for the icon when deactivated.
   * @param {string|null} iconBullet
   *   The class name for a bullet icon using the `icon-*` pattern.
   * @return {object}
   *   An HTML element containing the on/off input and its parts.
   */
  generateOnOffSwitch(
    label,
    activated = true,
    eventListeners = null,
    wrapperAttributesList = [],
    wrapperClassList = [],
    inputFirst = true,
    iconOn = this.classNames.classNameIconCheckboxChecked,
    iconOff = this.classNames.classNameIconCheckboxUnchecked,
    iconBullet = null
  ) {
    const {
      classNameCheckboxToggleWrapper,
      classNameCheckboxToggle,
      classNameInputActivated,
      classNameInputDeactivated,
      classNameInputWrapperActivated,
      classNameInputWrapperDeactivated,
      classNameIconWithinContent
    } = this.classNames;

    const checkboxUniqueId = this.generateUniqueIdentifier();

    // Create a wrapper div for the activation elements within the form.
    const wrapperDiv = document.createElement("div");
    Object.entries(wrapperAttributesList).forEach(([key, value]) => {
      wrapperDiv.setAttribute(key, value);
    });
    wrapperDiv.classList.add(
      ...wrapperClassList,
      classNameCheckboxToggleWrapper,
      activated
        ? classNameInputWrapperActivated
        : classNameInputWrapperDeactivated
    );

    // Create a checkbox input element for debugger activation
    const itemInput = document.createElement("input");
    itemInput.type = "checkbox";
    itemInput.id = checkboxUniqueId;
    itemInput.style.pointerEvents = "none";
    itemInput.classList.add(classNameCheckboxToggle);

    // Applies the initial controller state based on default value received.
    itemInput.checked = activated;

    // If requested, attach a 'click' event listener on the wrapper div.
    if (eventListeners !== null) {
      eventListeners.forEach(eventListener => {
        wrapperDiv.addEventListener(
          eventListener.eventListener,
          eventListener.eventCallback
        );
      });
      // wrapperDiv.addEventListener('click', clickEventListener);
    }

    // Attach default 'change' event listener to the checkbox.
    itemInput.addEventListener("change", () => {
      wrapperDiv.classList.toggle(classNameInputWrapperActivated);
      wrapperDiv.classList.toggle(classNameInputWrapperDeactivated);
    });

    // Create icons for the debugger activation checkbox.
    const createIconElement = iconClasses => {
      const iconElement = document.createElement("span");
      iconElement.style.pointerEvents = "none";
      iconClasses.forEach(iconClass => {
        iconElement.classList.add(iconClass);
      });
      return iconElement;
    };
    const iconSelectedTrue = createIconElement([
      iconOn,
      classNameInputActivated
    ]);
    const iconSelectedFalse = createIconElement([
      iconOff,
      classNameInputDeactivated
    ]);

    // Append the input element, as well as the selected and deselected icons.
    wrapperDiv.append(itemInput, iconSelectedTrue, iconSelectedFalse);

    // Create a label element for the debugger activation checkbox.
    if (label.length > 0) {
      const itemLabel = document.createElement("label");
      itemLabel.setAttribute("for", checkboxUniqueId);
      itemLabel.style.pointerEvents = "none";
      itemLabel.textContent = label;

      if (inputFirst) {
        wrapperDiv.appendChild(itemLabel);
      } else {
        wrapperDiv.insertBefore(itemLabel, wrapperDiv.firstChild);
      }
    }

    // Create a bullet icon if requested.
    if (iconBullet !== null) {
      const bulletIcon = createIconElement([
        iconBullet,
        classNameIconWithinContent
      ]);
      wrapperDiv.insertBefore(bulletIcon, wrapperDiv.firstChild);
    }

    return wrapperDiv;
  },

  /**
   * Filters all checked nodes.
   * @param {array} nodes
   *   An array of nodes to be analyzed.
   * @return {boolean}
   *   An array of nodes.
   */
  getCheckedNodes(nodes) {
    const { classNameInputWrapperActivated } = this.classNames;
    return nodes.filter(node => {
      return node.classList.contains(classNameInputWrapperActivated);
    });
  },

  /**
   * List of nodes with some sort of exposed cache.
   *
   * @param {array} nodes
   *   The array of node objects.
   * @return {array}
   *   A reduced array of nodes with cache.
   */
  getNodesWithCache(nodes) {
    return nodes.filter(node => node.instanceActiveElement.cacheHit);
  },

  /**
   * Gets a consolidated list of object types.
   * @param {array} nodes
   *   The array of node objects.
   * @return {object}
   *   An object with object types as keys, and the number of occurrences.
   */
  consolidateObjectTypes(nodes) {
    return nodes
      .map(node => node.instanceActiveElement.objectType)
      .reduce((acc, node) => {
        acc[node] = acc[node] || { count: 0 };
        acc[node].count += 1;
        return acc;
      }, {});
  },

  getFilteredNodesByObjectType(nodes, objectType) {
    return nodes.filter(
      node => node.instanceActiveElement.objectType === objectType
    );
  }
};
