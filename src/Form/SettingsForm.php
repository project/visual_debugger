<?php

namespace Drupal\visual_debugger\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\visual_debugger\VisualDebuggerConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Visual Debugger settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The cache tags invalidator service.
   *
   * @var Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * This is the confifgured key identifying `use_on_frontend`.
   *
   * @var string
   */
  private $useOnFrontend;

  /**
   * This is the confifgured key identifying `use_on_admin`.
   *
   * @var string
   */
  private $useOnAdmin;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    CacheTagsInvalidatorInterface $cache_tags_invalidator,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->useOnFrontend = VisualDebuggerConstants::IS_FRONTEND_KEY;
    $this->useOnAdmin = VisualDebuggerConstants::IS_ADMIN_KEY;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'visual_debugger_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['visual_debugger.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('visual_debugger.settings');

    $form[$this->useOnFrontend] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Visual Debugger on frontend theme'),
      '#default_value' => $config->get($this->useOnFrontend, TRUE),
    ];

    $form[$this->useOnAdmin] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Visual Debugger on admin theme'),
      '#default_value' => $config->get($this->useOnAdmin),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Original configuration.
    $config = $this->config('visual_debugger.settings');
    $original_frontend = (bool) $config->get($this->useOnFrontend);
    $original_admin = (bool) $config->get($this->useOnAdmin);

    // New configuration.
    $new_frontend = (bool) $form_state->getValue($this->useOnFrontend);
    $new_admin = (bool) $form_state->getValue($this->useOnAdmin);

    // Invalidate the cache tag if Frontend configuration is differing.
    if ($original_frontend != $new_frontend) {
      $this->cacheTagsInvalidator->invalidateTags([VisualDebuggerConstants::FRONTEND_TAG]);
    }

    // Invalidate the cache tag if Admin configuration is differing.
    if ($original_admin != $new_admin) {
      $this->cacheTagsInvalidator->invalidateTags([VisualDebuggerConstants::ADMIN_TAG]);
    }

    // Update configuration based on form values.
    $this->config('visual_debugger.settings')
      ->set(VisualDebuggerConstants::IS_FRONTEND_KEY, $new_frontend)
      ->set(VisualDebuggerConstants::IS_ADMIN_KEY, $new_admin)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
