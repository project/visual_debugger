const gulp = require("gulp");
const terser = require("gulp-terser");
const rename = require("gulp-rename");

gulp.task("minify", () => {
  return gulp
    .src("js/source/**/*.js")
    .pipe(terser())
    .pipe(rename({ extname: ".min.js" }))
    .pipe(gulp.dest("js/dist"));
});

gulp.task("watch", () => {
  gulp.watch("js/source/**/*.js", gulp.series("minify"));
});
